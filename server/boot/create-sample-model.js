'use strict';

module.exports = function(app) {
  app.dataSources.mongoDs.automigrate(['CoffeeShop', 'Product', 'Comment'], function(err) {
    if (err) throw err;

    app.models.CoffeeShop.create([{
      name: 'Bel Cafe',
      city: 'Vancouver'
    }, {
      name: 'Three Bees Coffee House',
      city: 'San Mateo'
    }, {
      name: 'Caffe Artigiano',
      city: 'Vancouver'
    }, ], function(err, coffeeShops) {
      if (err) throw err;

      app.models.Product.create([
        {
          name: 'Coffee with milk',
          type: 'coffee',
          coffeeShopId: coffeeShops[0].id
        }
      ])
      console.log('Models created: \n', coffeeShops);
    });
  });
};
